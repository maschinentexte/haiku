use tide::{Redirect, Request, Response, StatusCode};
use tide::http::mime;

use std::{collections::HashMap, error::Error};
use serde::Deserialize;

#[derive(Debug, Deserialize, Clone)]
struct Record {
    name: String,
    url: String,
}

fn read_config() -> Result<Config, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_path("./urls.csv")?;
    let mut config = Config {
        urls: HashMap::new()
    };
    for result in rdr.deserialize::<Record>() {
        let record = result?;
        println!("Record found {:?}", record);
        config.urls.insert(record.name.clone(), record);
    }
    Ok(config)
}

#[derive(Clone)]
struct Config {
    urls: HashMap<String, Record>,
}

async fn resolve(req: Request<Config>) -> tide::Result<Response> {
    let config : &Config = req.state();
    let key = req.param("key").unwrap();
    Ok(match config.urls.get(key) {
        Some(record) => Redirect::new(record.url.clone()).into(),
        None => Response::builder(StatusCode::NotFound)
            .content_type(mime::PLAIN)
            .body("Not Found")
            .build()
    })
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    let config = read_config().expect("Could not read config");

    let mut app = tide::with_state(config);

    app.at("/").get(|_| async {
        Ok("This is Haiku, a url shortener for a poetic web.")
    });

    app.at("/:key").get(resolve);

    app.listen("0.0.0.0:8000").await?;
    Ok(())
}
