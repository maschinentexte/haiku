# Based on the example from https://blog.logrocket.com/packaging-a-rust-web-service-using-docker/
FROM rust:latest as builder

RUN USER=root cargo new --bin haiku
WORKDIR /haiku
# COPY ./Cargo.toml ./Cargo.toml
# RUN cargo build --release
# RUN rm src/*.rs

ADD . ./

# RUN rm ./target/release/deps/haiku*
RUN cargo build --release


FROM debian:buster-slim
ARG APP=/haiku

RUN apt-get update \
    && apt-get install -y ca-certificates tzdata \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 8000

ENV TZ=Etc/UTC \
    APP_USER=murakami

RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${APP}

COPY --from=builder /haiku/target/release/haiku ${APP}

RUN chown -R $APP_USER:$APP_USER ${APP}

USER $APP_USER
WORKDIR ${APP}

CMD ["./haiku"]